export default {
  getAllEnts(state) {
    // console.log(state.ALL_ENTS.map((ele) => ele.val))
    return state.ALL_ENTS.map((ele) => ele.val);
  },
  getDefaultEnts(state) {
    return state.DEFAULT_ENTS;
  },
  getDefaultModel(state) {
    return state.DEFAULT_MODEL;
  },
};
