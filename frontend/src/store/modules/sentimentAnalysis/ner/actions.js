import { Notify } from "quasar";
import { api } from "boot/axios";

export default {
  async getDataset(context, payload) {
    let destination = "";
    if (context.state.earlyPhaseOn) {
      destination = "/annotation_scratch";
      // destination = "/annotation";
    } else {
      destination = "/annotation";
    }
    console.log('annotation destination: ', destination)
    const response = await api.get(destination);
    console.log('annotation response: ', response);

    if (response.status !== 200) {
      Notify.create({
        color: "negative",
        position: "top",
        message: "Loading failed",
        icon: "report_problem",
      });
    } else {
      context.commit("updateResult", response.data["ents"]);
      context.commit("updateDataIndex", response.data["index"]);
      context.commit("addHistory");
    }
  },
  async getSentimentData(context, payload) {
    let destination = "/sentimentAnalysis"
    const response = await api.get(destination);
    console.log('sentiment response: ', response);

    if (response.status !== 200) {
      Notify.create({
        color: "negative",
        position: "top",
        message: "Loading failed",
        icon: "report_problem",
      });
    } else {
      // context.commit("updateResult", response.data["ents"]);
      // context.commit("updateDataIndex", response.data["index"]);
      // context.commit("addHistory");
    }
  },
};
