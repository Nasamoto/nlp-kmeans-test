import getters from "./getters";
import mutations from "./mutations";
import actions from "./actions";
import {characterNames, surroundRadius} from "pages/sentimentAnalysis/data";

export default {
  namespaced: true,
  state() {
    return {
      //SVG Elements
      svg: null, mainGroupLeft: null, mainGroupRight: null, width: 1600, height: 940,

      //NLP Data
      data: null, allItem: [], curAllItem: null, wordsMap: [], curWordsMap: null, sparseMatrix: [], curSparseMatrix: null, IDMap: [], curIDMap: null, clusters: [], curCluster: null, Z: [], z: null,

      //scatter plot layout elements
      circles: [], Clusters: [], ClustersScatterPlot: null, clusterSign: null, signGroupsSpan: [], ScatterPlotFlag: 0,

      //scale groups and axis groups
      xScale: null, yScale: null, xScaleGroups: [], yScaleGroups: [], new_xScaleGroups: [], new_yScaleGroups: [], xAxisGroups: [], yAxisGroups: [],

      //event relevant elements
      selectedID: [], surroundDotsID: [], mouseoverDot: null, updatedSurroundRadius: surroundRadius,

      //LASSO
      lasso: null, lassoPath: '', lassoCoordinates: [], curLassoXScale: null, curLassoYScale: null, centroid: null, centroidID: null, isLassoing: false,

      //Selection Usage
      characterNames: characterNames, currentNameIndex: characterNames.length - 1, currentName: characterNames[this.currentNameIndex],

      //DetailTable Elements
      tbody: null, rows: null,

    };
  },
  getters,
  mutations,
  actions,
};
