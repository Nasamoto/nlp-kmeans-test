export const width = 1600
export const height = 940
export const margin = { top: height * 0.04, right: 10, bottom: 20, left: width / 2 * 0.03 };
//Parameters
export const eachMenuItem = 50, padding = eachMenuItem * 0.1, circleRadius = 4, zoomRate = 0.5, detailTextOffsetRate = 1.07
export const groupHeight = height * 0.9
export const attributeRectWidth = 10, clusterGroupTextLength = 13
export const plotWidth = width / 2 * 0.8, plotHeight = groupHeight / 2 * 0.85, selectBoxWidth = 80
export const xAxisPos = plotWidth / 16, yAxisPos = plotHeight, cxSpan = xAxisPos - attributeRectWidth, cySpan = 50, defaultWeight = 0
// const scatterPlotLimits = [40, 680, 20, 380]
const scatterPlotLimits = [xAxisPos + 1, plotWidth + xAxisPos - 1, margin.top / 2 + 1, plotHeight + margin.top / 2 -1]
export const originalOpacity = 0.6, OPACITY = 0.1, OPACITY_CLUSTER = 0.5, OPACITY_SELECTED = 1, plotRatio = 1.2, prefix = 'ID'
export let signGroupsSpan = [], scatterplotOutter

//File Parameters
export const UNSELECTED = 'ClustersCirclesUnselected', SELECTED = 'ClustersCirclesSelected'
export const hdbscanCluster = "hdbscan_predicted_cluster", kmeansCluster = 'kmeans_predicted_cluster'
export const ID = 'doc_id', X = 't-sne_x', Y = 't-sne_y', Sentence = 'assembled_sentence', Criteria1 = 'personality', Role = 'role', Cluster = 'cluster',
    Criteria2 = 'emotion', Criteria3 = 'tone', Contribute_Score = 'contributeScore', Contribute_Detail = 'Contribute_Detail'
// const Attributes = [ID, Sentence, Criteria1, Criteria2, Criteria3, Contribute_Score]
export const Attributes = [Role, Cluster, Sentence, Contribute_Score, ID]

//Groups

//Characters Parameters
export let ClustersScatterPlot, DetailTable, Clusters = [], circles = [], ScatterPlotFlag = 0, dragRect
let xAxisGroups = [], yAxisGroups = [],xScaleGroups = [], yScaleGroups = [], new_xScaleGroups = [], new_yScaleGroups = [],  DomainX = [], DomainY = [], xScale, yScale
export let allItem = [], curAllItem = [], clusters = [], curCluster, wordsMap = [], curWordsMap, sparseMatrix = [], curSparseMatrix,IDMap = [], curIDMap, Z = [], z
export let selectedID = [], surroundDotsID = [], surroundRadius = 1, updatedSurroundRadius = 20, mouseoverDot, zoomUpHighlightDotDis = 40, funky = false, lastSelectedLength = 0
export let lassoWidth = 2
export let all = 'all_roles', characterNames = ['Howard', 'Leonard', 'Penny', 'Sheldon', all], filePostFix = [4, 8, 5, 15, 30], postfix = '_min_sample_size_10--num_clusters_'
export let currentName, currentNameIndex = 0
export let table, thead, tbody, rows

function createCircleId(id) {
    return prefix + '_' + id + '_' + currentName
}

export const customizedColorPalette = {
    'Cluster-1': [240, 240, 240],
    'Cluster0': [166, 206, 227],
    'Cluster2': [178, 223, 138],
    'Cluster4': [251, 154, 153],
    'Cluster6': [253, 191, 111],
    'Cluster8': [202, 178, 214],
    'Cluster10': [255, 255, 153],
    'Cluster12': [102, 194, 166],
    'Cluster14': [141, 160, 203],
    'Cluster16': [167, 216, 84],
    'Cluster18': [229, 196, 148],
    'Cluster1': [31, 120, 180],
    'Cluster3': [51, 160, 44],
    'Cluster5': [227, 26, 28],
    'Cluster7': [255, 127, 0],
    'Cluster9': [106, 61, 154],
    'Cluster11': [177, 89, 40],
    'Cluster13': [252, 141, 98],
    'Cluster15': [232, 138, 196],
    'Cluster17': [255, 217, 48],
    'Cluster19': [150, 150, 150]
};