from flask import Flask, request, jsonify
from flask_restful import Api
from flask_cors import CORS
from scipy.spatial import distance
from sklearn.cluster import KMeans
import numpy as np

# from db import db

# from resources.spacy_service import SpacyService
# from resources.huggingface_service import HuggingFaceService
# from resources.annotation_service import AnnotationServiceScratch, AnnotationService
from resources.sentimentAnalysis import SentimentAnalysis

app = Flask(__name__)
app.config['MONGOALCHEMY_DATABASE'] = 'myDatabase'
app.config['PROPAGATE_EXCEPTIONS'] = True
app.secret_key = 'neraBackend'
CORS(app, resources={r"/*": {"origins": "*"}})
api = Api(app)

# api.add_resource(SpacyService, "/spacy")
# api.add_resource(HuggingFaceService, "/huggingface")
# api.add_resource(AnnotationService, "/annotation")
# api.add_resource(AnnotationServiceScratch, "/annotation_scratch")
api.add_resource(SentimentAnalysis, '/sentimentAnalysis')


@app.route('/find_centroid', methods=['POST'])
def find_centroid():
    if request.method == 'POST':
        try:
            clusterData = request.get_json()
            # print(type(clusterData), clusterData)
            # result = process_cluster_data(clusterData)
            centroid_id = find_centroid_by_kmeans(clusterData)
            print(centroid_id)
            return jsonify(centroid_id), 200

        except Exception as e:
            return jsonify({"error": str(e)}), 500
    else:
        # Handle a GET request if needed
        # This part may be different depending on your use case
        return "GET request received", 200
    
    
def find_centroid_by_kmeans(cluster_points):
    if not cluster_points:
        return None

    data = np.array([[float(point['x']), float(point['y'])] for point in cluster_points])
    k = 1  # Number of clusters
    kmeans = KMeans(n_clusters=k).fit(data)
    centroids = kmeans.cluster_centers_

    min_distance = float('inf')
    centroid_id = None
    for obj in cluster_points:
        x = (float(obj['x']) - centroids[0][0]) ** 2
        y = (float(obj['y']) - centroids[0][1]) ** 2
        distance_to_centroid = np.sqrt(x + y)
        if distance_to_centroid < min_distance:
            min_distance = distance_to_centroid
            centroid_id = obj['id']

    return centroid_id

if __name__ == '__main__':
    # We plan to use database at first, but it turns out to be unnecessary
    # db.init_app(app)
    app.run(host="0.0.0.0", port=5000, debug=True)
