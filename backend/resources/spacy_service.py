import spacy
from flask_restful import Resource, reqparse

# Define a Flask RESTful resource class called SpacyService:
# This class extends the Resource class from Flask-RESTful and represents a resource that can handle HTTP requests.


class SpacyService(Resource):
    # reqparse.RequestParser() is used to parse incoming HTTP request arguments, including "text", "model" and "mode".
    parser = reqparse.RequestParser()
    parser.add_argument("text", type=str, help="Text", required=True)
    parser.add_argument("model", type=str, help="Model", required=True)
    parser.add_argument("mode", type=str, help="Mode", required=True)

    print("Loading...")
    MODELS = {
        "en_core_web_sm": spacy.load("en_core_web_sm"),
        # "en_core_web_md": spacy.load("en_core_web_md"),
        # "en_core_web_lg": spacy.load("en_core_web_lg"),
        # "en_core_web_trf": spacy.load("en_core_web_trf"),
    }

    print("Loaded!")

    def put(self):
        """Get entities for displaCy ENT visualizer."""

        args = SpacyService.parser.parse_args()
        nlp = self.MODELS[args.model]

        print(args.text)

        doc = nlp(args.text)
        if args.mode == 'char':
            res = [{"token": ent.text, "start": ent.start_char, "end": ent.end_char, "label": ent.label_}
                   for ent in doc.ents]
        elif args.mode == 'token':
            res = [{"token": token.text, "label": token.ent_type_ if token.ent_type_ else 'null', "iob": token.ent_iob}
                   for token in doc]

        return {"text": doc.text, "ents": res}, 201

# Here's how you can use this API:
#
# Send a PUT request to the specified endpoint ("/your_endpoint") with the required parameters: "text," "model," and "mode."
# The API will process the input text using the specified spaCy model and mode.
# The extracted information (entities or tokens) is returned in the response.
# You would need to integrate this API with a Flask application and specify the endpoint where it will be accessible. Additionally,
# ensure that you have the spaCy library and models installed in your environment.