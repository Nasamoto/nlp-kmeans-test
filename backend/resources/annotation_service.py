from flask_restful import Resource, reqparse

from models.al_model import ALEngine


class AnnotationService(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument("index", type=int, help="Data Index", required=True)
    parser.add_argument("label", type=str, help="label", required=True)
    parser.add_argument("annotator", type=str, help="annotator", required=True)

    al_engine = ALEngine(
        "elastic/distilbert-base-uncased-finetuned-conll03-english")

    #In the get method, it retrieves data for annotation using self.al_engine.next2label(). It returns the data along with their indices in the response.
    def get(self):
        """Get entities for displaCy ENT visualizer."""

        index, res = self.al_engine.next2label()

        return {"ents": res, "index": index}, 200

    #In the put method, it parses the incoming parameters, and then it calls self.al_engine.update_dataset() to update the dataset with the provided label and annotator information.
    def put(self):
        args = AnnotationService.parser.parse_args()
        self.al_engine.update_dataset(args.index, args.label, args.annotator)

        return {"index": args.index}, 201


class AnnotationServiceScratch(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument("index", type=int, help="Data Index", required=True)
    parser.add_argument("label", type=str, help="label", required=True)
    parser.add_argument("annotator", type=str, help="annotator", required=True)

    al_engine = ALEngine('distilbert-base-uncased', need_train=True)

    def get(self):
        """Get entities for displaCy ENT visualizer."""

        index, res = self.al_engine.next2label()

        return {"ents": res, "index": index}, 200

    def put(self):
        args = AnnotationService.parser.parse_args()
        print(args.index, args.label, args.annotator)
        self.al_engine.update_dataset(args.index, args.label, args.annotator)

        return {"index": args.index}, 201

# These resources seem to be part of an annotation system where you can retrieve data for annotation, and when you annotate data,
# it updates the dataset. The specific models used (elastic/distilbert-base-uncased-finetuned-conll03-english and distilbert-base-uncased) appear to be models
# for text classification or entity recognition tasks.
# Make sure that you have the required dependencies and the ALEngine model correctly defined in your project.
# Also, ensure that the routes for these resources are correctly set up in your Flask application (as seen in your main script).