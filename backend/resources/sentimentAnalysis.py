from flask import jsonify
from flask_restful import Resource, reqparse
import pandas as pd
import json
import csv
import os

script_dir = os.path.dirname(os.path.abspath(__file__))
print('script_dir: ', script_dir)
dir_path = script_dir + '\\'


class SentimentAnalysis(Resource):
    roles_file_path = dir_path + "all_roles.csv"
    vocabulary_file_path = dir_path + "all_roles_vocabulary.json"
    weights_file_path = dir_path + "all_roles_weights.csv"

    def __init__(self):
        # Initialize a request parser
        self.parser = reqparse.RequestParser()
        # Define the expected input fields
        self.parser.add_argument('type', type=str, required=True)

    def put(self):
        """Get entities for displaCy ENT visualizer."""
        try:
            args = self.parser.parse_args()
            TYPE = args['type']
            data = []
            all_roles = pd.read_csv(self.roles_file_path)
            with open(self.roles_file_path, 'r', newline='') as csv_file:
                csv_reader = csv.DictReader(csv_file)
                for row in csv_reader:
                    data.append(row)

            with open(self.vocabulary_file_path, 'r') as json_file:
                vocabulary = json.load(json_file)
            weights = pd.read_csv(self.weights_file_path)

            all_roles_json = all_roles.to_dict(orient='records')
            weights_json = weights.to_dict(orient='records')

            # print('data', data)
            # print('all_roles ', all_roles)

            # return jsonify(data)
            response_data = {
                "all_roles": all_roles_json,
                "vocabulary": vocabulary,
                "weights": weights_json
            }

            if TYPE == "0":
                return jsonify(data)
            elif TYPE == "1":
                return vocabulary, 200
            else:
                return weights_json, 200

        except Exception as e:
            return {"message": "An error occurred while processing the request.", "error": str(e)}, 500
