import os
import sys
import json
import datetime
import types
from collections import defaultdict
from copy import deepcopy, copy

import torch
import scipy
import numpy as np
from spacy.cli import evaluate

from torch.utils.data.dataloader import DataLoader

from datasets import load_dataset, load_metric

from transformers import AutoTokenizer, AutoModelForTokenClassification, TrainingArguments
from transformers import DataCollatorForTokenClassification

from baal.active.dataset import ActiveLearningDataset
from baal.active.dataset.pytorch_dataset import ActiveLearningPool

from baal.transformers_trainer_wrapper import BaalTransformersTrainer

from baal.active.heuristics.heuristics import AbstractHeuristic
from datasets import list_datasets

available_datasets = list_datasets()
torch.autograd.set_detect_anomaly(True)


use_cuda = torch.cuda.is_available()
print("CUDA availability: {}".format(use_cuda))
# The load_metric function is used to load a metric for evaluating the performance of a sequence labeling (NER) model.
# It is typically used for evaluating the accuracy of NER predictions.
metric = load_metric("seqeval")
# metric = evaluate.load('seqeval')

DEFAULT_LABEL = ['O', 'B-PER', 'I-PER', 'B-ORG',
                 'I-ORG', 'B-LOC', 'I-LOC', 'B-MISC', 'I-MISC']
BIO_MAP = {
    "B": 3,
    "I": 1,
    "O": 2
}
# MAX_STEPS defines the maximum number of training steps for a model during training. It can be used to limit the training process.
MAX_STEPS = 100
# NUM_LABEL_RETRAIN_MODEL specifies the number of label samples required to retrain a model. It may be part of a dynamic training strategy.
NUM_LABEL_RETRAIN_MODEL = 5
# MAX_SAMPLES defines the maximum number of training samples to be used. It can be used to limit the training dataset size.
MAX_SAMPLES = 5000
# MAX_CANDIDATES may specify the maximum number of candidates considered during some processing.
MAX_CANDIDATES = 50
# RANDOM_LABELS may specify the number of randomly labeled examples used during training or evaluation, which could be part of an active learning strategy.
RANDOM_LABELS = 250


# This function for tokenizing and aligning labels in a dataset using the Hugging Face Transformers library,
# specifically for the CoNLL-2003 dataset, which is a popular dataset for named entity recognition (NER) tasks.
def get_tokenized_dataset(tokenizer):
    if "conll2003" in available_datasets:
        print("The 'conll2003' dataset is available.")
    else:
        print("The 'conll2003' dataset is not available.")

    datasets = load_dataset("conll2003")
    label_all_tokens = True

    #  It is used to tokenize the text and align labels for NER.
    # It iterates through examples in the dataset and tokenizes the input text using the provided tokenizer.
    # The is_split_into_words flag is set to True to ensure that the tokenizer splits text into words.
    def tokenize_and_align_labels(examples):
        tokenized_inputs = tokenizer(
            examples["tokens"], truncation=True, is_split_into_words=True)

        labels = []
        word_ids_list = []
        text = []
        annotators = []
        for i, label in enumerate(examples["ner_tags"]):
            word_ids = tokenized_inputs.word_ids(batch_index=i)
            previous_word_idx = None
            label_ids = []
            for word_idx in word_ids:
                # Special tokens have a word id that is None. We set the label to -100 so they are automatically ignored in the loss function.
                # Labels are aligned with the tokens. Special tokens (e.g., [CLS], [SEP]) are assigned a label of -100 to ignore them in the loss function.
                if word_idx is None:
                    label_ids.append(-100)
                # We set the label for the first token of each word.
                elif word_idx != previous_word_idx:
                    label_ids.append(label[word_idx])
                # For the other tokens in a word, we set the label to either the current label or -100, depending on
                # the label_all_tokens flag.
                else:
                    label_ids.append(
                        label[word_idx] if label_all_tokens else -100)
                previous_word_idx = word_idx

            labels.append(label_ids)
            word_ids_list.append(word_ids)
            text.append(" ".join(examples["tokens"][i]))
            annotators.append("default")

        tokenized_inputs["labels"] = labels
        tokenized_inputs["word_ids"] = word_ids_list
        tokenized_inputs["text"] = text
        tokenized_inputs["annotator"] = annotators
        return tokenized_inputs

    #  The datasets.map method is used to apply the tokenize_and_align_labels function to all examples in the dataset.
    #  This is done in batches for efficiency.
    tokenized_datasets = datasets.map(tokenize_and_align_labels, batched=True)
    # The code sets the format of the tokenized datasets by specifying the desired columns, including input_ids, attention_mask, labels, word_ids, and tokens.
    tokenized_datasets.set_format(
        columns=['input_ids', 'attention_mask', 'labels', 'word_ids', 'tokens'])
    return tokenized_datasets


# This code is useful for preparing a dataset for training a NER model using Hugging Face's Transformers library.
# The tokenized dataset is structured to work seamlessly with the library's NER models for fine-tuning and training.


# This function is used to compute evaluation metrics for a named entity recognition (NER) task,
# given the model's predictions and the corresponding labels.
def compute_metrics(p, label_list, details=False):
    # p: This argument is expected to be a tuple containing two elements: the model's predictions and the true labels.
    # label_list: This is a list of labels used in the NER task.
    # details: A boolean flag that indicates whether to return detailed metrics or a summary.

    # Computing Predictions: The code first extracts the predictions from the p argument and converts them into a format
    # where the label with the highest probability is selected for each token in each sequence.
    predictions, labels = p
    predictions = np.argmax(predictions, axis=2)

    # In NER tasks, special tokens (e.g., [CLS], [SEP]) are often assigned an "ignored index" (e.g., -100) and should be excluded from the evaluation.
    # The code filters out these special tokens from both the predicted and true label sequences.
    true_predictions = [
        [label_list[p] for (p, l) in zip(prediction, label) if l != -100]
        for prediction, label in zip(predictions, labels)
    ]
    true_labels = [
        [label_list[l] for (p, l) in zip(prediction, label) if l != -100]
        for prediction, label in zip(predictions, labels)
    ]

    # The metric.compute function is called to compute evaluation metrics for the NER task.
    # This function calculates various metrics such as precision, recall, F1 score, and accuracy.
    results = metric.compute(
        predictions=true_predictions, references=true_labels)

    # Depending on the details flag, the function returns either a summary of
    # key metrics (precision, recall, F1, and accuracy) or the detailed metric results.
    if details:
        return results
    else:
        return {
            "precision": results["overall_precision"],
            "recall": results["overall_recall"],
            "f1": results["overall_f1"],
            "accuracy": results["overall_accuracy"],
        }


# This class is designed to create a new dataset from unlabelled samples,
# which is typically used in active learning scenarios where you iteratively select and
# label data points to improve a machine learning model's performance.
class HuggingFaceActiveLearningDataset(ActiveLearningDataset):
    @property
    def pool(self):
        """Returns a new Dataset made from unlabelled samples.
        Raises:
            ValueError if a pool specific attribute cannot be set.
        """
        pool_dataset = copy(self._dataset)

        for attr, new_val in self.pool_specifics.items():
            if hasattr(pool_dataset, attr):
                setattr(pool_dataset, attr, new_val)
            else:
                raise ValueError(f"{pool_dataset} doesn't have {attr}")

        pool_dataset = pool_dataset.select(
            (~self.labelled).nonzero()[0].reshape([-1]))
        ald = ActiveLearningPool(
            pool_dataset, make_unlabelled=self.make_unlabelled)
        return ald


# It includes methods for predicting on a dataset and customizing the data loader for training.
class TokenALTransformersTrainer(BaalTransformersTrainer):
    def predict_on_dataset(self,
                           dataset,
                           iterations: int = 1,
                           half: bool = False,
                           ignore_keys=None):
        """
        Use the model to predict on a dataset `iterations` time.
        Args:
            dataset (Dataset): Dataset to predict on.
            iterations (int): Number of iterations per sample.
            half (bool): If True use half precision.
            ignore_keys (Optional[List[str]]): A list of keys in the output of your model
                (if it is a dictionary) that should be ignored when gathering predictions.
        Notes:
            The "batch" is made of `batch_size` * `iterations` samples.
        Returns:
            Array [n_samples, n_outputs, ..., n_iterations].
        """
        dataset = deepcopy(dataset._dataset)
        dataset.set_format(
            columns=['input_ids', 'attention_mask', 'labels'])
        # It iteratively predicts on the dataset using the predict_on_dataset_generator method and collects the predictions in the preds list.
        preds = list(self.predict_on_dataset_generator(dataset=dataset,
                                                       iterations=iterations,
                                                       half=half,
                                                       ignore_keys=ignore_keys))

        seq_lens = [len(ex['attention_mask']) for ex in dataset]
        idx = 0
        probs = []

        # It processes the predictions, calculating softmax probabilities and trimming them to the original sequence lengths.
        for pred in preds:
            pred = scipy.special.softmax(pred.squeeze(-1), axis=-1)
            for i in range(pred.shape[0]):
                seq_len = seq_lens[idx + i]
                probs.append(pred[i][:seq_len])
            idx += pred.shape[0]
        # The result is a list of probabilities for each sample in the dataset, where each entry in the list corresponds
        # to a different output dimension. The shape of the result is [n_samples, n_outputs, ..., n_iterations].
        return probs

    def get_train_dataloader(self):
        dataset = deepcopy(self.train_dataset)
        # sets the format of the dataset to include specific columns 'input_ids', 'attention_mask', and 'labels'
        dataset._dataset.set_format(
            columns=['input_ids', 'attention_mask', 'labels'])
        # It constructs a data loader with the specified batch size, data collator, number of worker processes, and pin memory settings.
        return DataLoader(
            dataset,
            batch_size=self.args.train_batch_size,
            collate_fn=self.data_collator,
            num_workers=self.args.dataloader_num_workers,
            pin_memory=self.args.dataloader_pin_memory,
        )


# MNLP  appears to be part of a heuristic-based selection strategy, possibly used in active learning
class MNLP(AbstractHeuristic):
    """
    Sort by the highest acquisition function value.
    Args:
        shuffle_prop (float): Amount of noise to put in the ranking. Helps with selection bias
            (default: 0.0).
        reduction (Union[str, callable]): function that aggregates the results
            (default: 'none`).
    """

    # It takes two parameters, shuffle_prop and reduction, which can be used to control the behavior of the heuristic.
    def __init__(self, shuffle_prop=0.0, reduction='none'):
        super().__init__(
            shuffle_prop=shuffle_prop, reverse=False, reduction=reduction
        )

    # This method computes scores for each data point based on the provided predictions. The input parameter predictions is expected to be a NumPy array
    # of prediction values. For each prediction in the array, the code calculates a score as the mean of the natural logarithm of the maximum value in the
    # prediction array. This implies that data points with higher confidence in their predictions (higher maximum probability) receive higher scores.
    def get_ranks(self, predictions):
        """
        Compute the score according to the heuristic.
        Args:
            predictions (ndarray): Array of predictions
        Returns:
            Array of scores.
        """
        scores = []
        # For each prediction in the input array predictions, the code computes a score as the mean of the
        # natural logarithm of the maximum value in the prediction array (the prediction with the highest probability).
        for pred in predictions:
            # print('pred: ', pred)
            scores.append(np.mean(np.log(pred.max(-1))))
            # print('scores: ', scores)

        scores = np.array(scores)
        # print('scores: ', scores)
        print('reorder: ', self.reorder_indices(scores), type(self.reorder_indices(scores)), isinstance(self.reorder_indices(scores), np.ndarray))
        # The purpose of this method is to reorder the indices of the data points based on the computed scores.
        return self.reorder_indices(scores)


# While this component calculates scores for data points, it's just one part of an active learning strategy.

# Selecting data points with the highest scores (in this case, using the MNLP scores).
# Labeling selected data points.
# Updating the model with the newly labeled data.
# Repeating the process iteratively.

# The specific active learning strategy would also depend on how these scores are used to select data points for labeling
# and how the model is updated based on the labeled data.You would typically integrate this component into a larger active learning framework,

class InformativenessHeuristic(AbstractHeuristic):
    """
    Sort by the highest informativeness score.
    Args:
        shuffle_prop (float): Amount of noise to put in the ranking. Helps with selection bias
            (default: 0.0).
        reduction (Union[str, callable]): function that aggregates the results
            (default: 'none`).
    """

    def __init__(self, shuffle_prop=0.0, reduction='none'):
        super().__init__(
            shuffle_prop=shuffle_prop, reverse=True, reduction=reduction
        )

    def calculate_informativeness(self, feature_vector):
        """
        Calculate informativeness based on a custom metric.
        Args:
            feature_vector (ndarray): Feature vector of a token.
        Returns:
            Informativeness score.
        """
        # Define your custom informativeness metric here.
        # For example, you can use a distance-based metric or any other relevant measure.
        # The higher the score, the more informative the token.

        # Example: Using the inverse of the Euclidean distance as informativeness
        euclidean_distance = np.linalg.norm(feature_vector)
        informativeness_score = 1.0 / (1.0 + euclidean_distance)

        return informativeness_score

    def get_ranks(self, feature_vectors):
        """
        Rank the feature vectors according to their informativeness.
        Args:
            feature_vectors (ndarray): Array of feature vectors.
        Returns:
            Array of ranks.
        """
        informativeness_scores = [self.calculate_informativeness(vec) for vec in feature_vectors]
        return self.reorder_indices(informativeness_scores)



# --------------------------------------------------------------------------------------------------
# This class is involved in active learning for a named entity recognition (NER) task and appears to
# manage the process of fine-tuning a model and selecting examples for labeling in an active learning loop.
class ALEngine():
    # the class initializes various components, including a tokenizer, model, training configuration, and data structures.
    def __init__(self, model_name, label_list=DEFAULT_LABEL, need_train=False):
        # Hard Code the dataset and model
        self.label_list = label_list
        self.label_map = {label: idx for idx, label in enumerate(label_list)}
        # The code initializes a tokenizer (self.tokenizer) and a token classification model (self.hf_model). The model is loaded from a pre-trained
        # checkpoint (model_name) and is set up for token classification, with the number of labels determined by the length of label_list.
        self.tokenizer = AutoTokenizer.from_pretrained(
            model_name)
        self.hf_model = AutoModelForTokenClassification.from_pretrained(
            model_name, num_labels=len(label_list))

        # self.init_weights is used to store the initial weights of the model. These weights are copied to retain
        # the model's original state for later use, such as resetting the model to its initial state.
        self.init_weights = deepcopy(self.hf_model.state_dict())

        # Obtain tokenized datasets for active learning. It likely tokenizes the data using the self.tokenizer.
        self.tokenized_datasets = get_tokenized_dataset(self.tokenizer)

        # An active learning dataset (self.active_set) is created using the tokenized validation dataset.
        # A heuristic for selecting examples to label (self.heuristic) is set up, with a shuffle_prop value of 0.5
        self.active_set = HuggingFaceActiveLearningDataset(
            self.tokenized_datasets["validation"])
        self.heuristic = MNLP(shuffle_prop=0.5)

        # self.data_collator is set up for token classification tasks. It likely prepares the data for model training
        self.data_collator = DataCollatorForTokenClassification(self.tokenizer)
        self.training_args = TrainingArguments(
            # output directory
            output_dir='./tmp/{}'.format(
                datetime.datetime.now().strftime("%Y%m%d%H%M%S")),
            max_steps=MAX_STEPS,  # total # of training steps per AL step
            per_device_train_batch_size=32,  # batch size per device during training
            per_device_eval_batch_size=64,  # batch size for evaluation
            weight_decay=0.01,  # strength of weight decay
            logging_dir='./tmp/{}'.format(
                datetime.datetime.now().strftime("%Y%m%d%H%M%S")),
        )
        # It sets up an ALTrainer for training the model with active learning.model:
        # The token classification model (self.hf_model) that will be trained.
        # args: Training arguments (self.training_args) that define how the model will be trained, including settings like the output directory and batch sizes.
        # train_dataset: The active learning dataset (self.active_set) that will be used for training.
        # tokenizer: The tokenizer (self.tokenizer) for processing text data.
        # data_collator: The data collator (self.data_collator) for organizing and batching data.
        # compute_metrics: A function (likely named compute_metrics) for calculating evaluation metrics during training.
        self.al_trainer = TokenALTransformersTrainer(model=self.hf_model,
                                                     args=self.training_args,
                                                     train_dataset=self.active_set,
                                                     tokenizer=self.tokenizer,
                                                     data_collator=self.data_collator,
                                                     compute_metrics=compute_metrics)
        self.get_probabilities = self.al_trainer.predict_on_dataset

        # It initializes data structures for managing labeled examples, such as candidates, new_label_count, and max_sample
        self.candidates = []  # maintain a list of example index for label
        self.new_label_count = 0  # A count of the newly labeled examples.
        self.max_sample = MAX_SAMPLES  # A maximum limit on the number of samples.
        self.ndata_to_label = MAX_CANDIDATES  # The maximum number of candidates that can be selected for labeling.

        # It sets up logging and a dictionary for recording performance metrics.
        if not os.path.exists("log"):
            os.makedirs("log")
        self.log_file = "log/al{}".format(
            datetime.datetime.now().strftime("%Y%m%d%H%M%S"))
        self.res_dict = defaultdict(list)

        if need_train:
            self.active_set.label_randomly(RANDOM_LABELS)
            self.update_model()

        self.log_performance()
        self.update_candidates()
        print("AL Engine Initialized!")
        sys.stdout.flush()

    def log_performance(self):
        print("Log performance!")
        # res = self.al_trainer.evaluate(self.tokenized_datasets["test"])
        # for k, v in res.items():
        #     self.res_dict[k].append(v)
        # self.res_dict["train_num"].append(len(self.active_set))

        # with open(self.log_file, 'w') as jsonfile:
        #     json.dump(self.res_dict, jsonfile)

    # This method post-processes model predictions to convert them into human-readable NER tags.
    def postprocess(self, token, word_ids, predictions):
        res = []
        previous_word_idx = None
        # It extracts the highest confidence label for each token in a sequence and converts it to an IOB (Inside, Outside, Beginning) NER tag.
        for word_idx, prediction in zip(word_ids, predictions[0]):
            if word_idx is None:
                continue
            elif word_idx != previous_word_idx:
                pred = prediction.argmax(-1).numpy()
                label = self.label_list[pred]
                cur_label = "null" if pred == 0 else label[2:]
                iob = 2 if pred == 0 else BIO_MAP[label[0]]
                res.append({"token": token[word_idx], "label": cur_label, "iob": iob,
                            "confidence": prediction[pred].detach().numpy().tolist()})
            previous_word_idx = word_idx

        return res

    # This method selects the next example to be labeled
    # It selects an example from the active learning dataset (active_set), and processes its tokens and predictions using the postprocess method.
    def next2label(self):
        if len(self.candidates) == 0:
            self.update_candidates()
        index = int(self.candidates[0])
        self.candidates = self.candidates[1:]

        inp = self.active_set._dataset[index]
        tokens = self.active_set._dataset["tokens"][index]
        word_ids = self.active_set._dataset["word_ids"][index]

        inp_cols = ['input_ids', 'attention_mask', 'labels']
        if use_cuda:
            inp = {k: torch.tensor(v).unsqueeze(0).to("cuda")
                   for k, v in inp.items() if k in inp_cols}
        else:
            inp = {k: torch.tensor(v).unsqueeze(0)
                   for k, v in inp.items() if k in inp_cols}

        pred = self.al_trainer.model(**inp).logits.softmax(-1).cpu()
        res = self.postprocess(tokens, word_ids, pred)
        # It returns the index of the selected example and the processed NER tags for that example.
        return index, res

    # This method updates the list of candidates for labeling.
    # It selects candidates from the active learning pool based on a heuristic (related to prediction confidence)
    def update_candidates(self):
        pool = self.active_set.pool
        print('update_candidates: ', len(self.active_set.pool))
        print('max samples: ', self.max_sample, len(pool), pool._dataset)
        if len(pool) > 0:
            # Limit number of samples
            if self.max_sample != -1 and self.max_sample < len(pool):
                indices = np.random.choice(
                    len(pool), self.max_sample, replace=False)
                pool._dataset = pool._dataset.select(indices)
            else:
                indices = np.arange(len(pool))
        print('indices:', indices)
        probs = self.get_probabilities(pool)
        print('probs is not None: ', probs is not None)
        if probs is not None and (isinstance(probs, types.GeneratorType) or len(probs) > 0):
            print('to_label: ', self.heuristic.get_ranks(probs))
            to_label = self.heuristic(probs)
            to_label = self.heuristic.get_ranks(probs)
            print('to_label_1', to_label)
            if indices is not None:
                to_label = indices[np.array(to_label)]
                print('to_label_2', to_label)
            if isinstance(to_label, (list, np.ndarray)) and len(to_label) > 0:
                self.candidates = to_label[: self.ndata_to_label]
                return True


    # This method updates the labeled dataset with a new set of labels.
    # It takes an index, new labels, and an annotator (defaulting to "admin").
    def update_dataset(self, index, new_label, annotator="admin"):
        def dataset_map(example, idx):
            if idx != index:
                return {}
            else:
                label_all_tokens = True
                previous_word_idx = None
                label_ids = []
                for word_idx in example["word_ids"]:
                    # Special tokens have a word id that is None. We set the label to -100 so they are automatically
                    # ignored in the loss function.
                    if word_idx is None:
                        label_ids.append(-100)
                    # We set the label for the first token of each word.
                    elif word_idx != previous_word_idx:
                        label_ids.append(new_label[word_idx])
                    # For the other tokens in a word, we set the label to either the current label or -100, depending on
                    # the label_all_tokens flag.
                    else:
                        label_ids.append(
                            new_label[word_idx] if label_all_tokens else -100)
                    previous_word_idx = word_idx

                if len(example["labels"]) != len(label_ids):
                    raise ValueError(
                        "Label Length Mismatch! Original: {} - New: {}".format(len(example["labels"]), len(label_ids)))
                return {"labels": label_ids, "annotator": annotator}

        # It processes the new labels to convert them to the model's label format.
        # It updates the labels for the selected example in the dataset.
        # It counts the number of newly labeled examples and triggers model retraining when needed.
        new_label = self.process_new_label(new_label)
        self.active_set._dataset = self.active_set._dataset.map(
            dataset_map, with_indices=True)
        self.active_set.label([index])
        self.new_label_count += 1
        if self.new_label_count >= NUM_LABEL_RETRAIN_MODEL:
            self.update_model()
            self.new_label_count = 0
        return True

    # This method processes new labels provided as a JSON string and converts them into model-compatible labels.
    # It maps labels to the corresponding label IDs used in the model.
    def process_new_label(self, new_label):
        labels = []
        new_label = json.loads(new_label)

        for label in new_label:
            if label["label"] == "null":
                labels.append("O")
            else:
                labels.append(
                    "{}-{}".format("IOB"[label["iob"] - 1], label["label"]))
        new_lab = [self.label_map[label] for label in labels]
        return new_lab

    # This method reinitializes the model's weights to their initial state
    # and re-trains the model from scratch using active learning data.
    def update_model(self):
        print("Retrain model from scratch!")
        sys.stdout.flush()
        self.al_trainer.load_state_dict(self.init_weights)
        self.al_trainer.train()
        self.log_performance()

# The ALEngine class serves as the central component for managing the active learning process,
# fine-tuning the model, and selecting examples for labeling. It appears to be designed for use
# in an NER task, where labeled examples are iteratively incorporated into the training process
